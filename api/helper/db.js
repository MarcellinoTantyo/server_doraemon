'use strict';

var mysql = require('mysql');

var connection = module.exports = mysql.createConnection({
	host		: 'localhost',
	user		: 'root',
	password	: '',
	database	: 'doraemon',
	dateStrings	: 'date',
	debug		: false
});

connection.connect(function(err){
	if (err) throw err
		console.log('Database connected');
});

connection.on('error', function(err){
	console.log(err.code);
});

exports.print_error = function(msg){
	res.send(msg);
}