'use strict';

var ejwt = require('express-jwt');

module.exports = {
	//error handler for express jwt
	error_handler : function(err, req, res, next){
		if (err.name === 'UnauthorizedError') {
			res.status(err.status).send({error:1, message:err.message});
		}
		console.log(err.message);
	},

	//check jwt is provided
	jwt_exist : [
	ejwt({
		secret : _config.jwt.secret,
		credentialsRequired : true
	}),
	function(err, req, res, next){
		if (err.name === 'UnauthorizedError') {
			res.status(err.status).send({error:1, message:err.message});
		}
		console.log(err.message);
	}
	]
}