const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

module.exports = {
	//custom error handler for express-validator
	error_handler : function(req, res, next){
		var errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(422).json({error: errors.mapped()});
		}
		next();
	}
}