'use strict';

module.exports = function(app){
	const{check, validationResult} = require('express-validator/check');
	const{matchedData, sanitize} = require('express-validator/filter');

	var login = require('../controllers/login_controller');
	var gadget_cards = require('../controllers/gadget_cards_controller');
	var game = require('../controllers/game_controller');
	var app_version = require('../controllers/app_version_controller');

	// Welcome
	app.route('/api')
	.get(function(req, res){res.send({message: 'Doraemon api server'})});

	// Login
	app.route('/api/login')
	.post(login.login);

	// Gadget cards
	app.route('/api/gadget-cards')
	.post(gadget_cards.gadget_cards);
	app.route('/api/check-qrid')
	.post(gadget_cards.check_qrid);

	// Game
	app.route('/api/game-seed')
	.post(game.game_seed);
	app.route('/api/game-start')
	.post(game.game_start);
	app.route('/api/game-end')
	.post(game.game_end);

	// Apps version
	app.route('/api/app-version')
	.get(app_version.app_version);
};