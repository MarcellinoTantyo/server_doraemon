'use strict';

const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

const db = require('../helper/db');
var jwt = require('jsonwebtoken');
var ejwt = require('express-jwt');
var jwt_helper = require('../helper/jwt');
var error_helper = require('../helper/error');
var randomstring = require('randomstring');

// make qrid batch from data
exports.qrid2 = function(req, res, next){

	var data = []
	var batch = req.body.batch;
	var card_type = req.body.card_type;
	var qty = req.body.qty;
	// console.log(res.locals.num)
	for (var ii = 0; ii < qty; ii++) {
		// console.log(ii)
		var card_type_a = card_type.substring(0,2);
		var card_type_b = card_type.substring(2,4);

		var random = randomstring.generate({
			length: 10,
			capitalization: 'uppercase'
		});
		var qrid1 = batch + card_type_a + random + card_type_b;

		// make character 19 and 20 qrid
		var char = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

		var x1 = 0;
		var x2 = 0;

		for (var i = 0; i < 18; i++) {

			var key = char.indexOf(qrid1[i]);

			if ((i + 1) % 2 == 1) {
				x1 += (key << 2) + 1;
			}else{
				x2 += (key << 1);
			}

		}

		var c = char.length;

		var value1 = (x1 + x2) % c;
		var value2 = x2 % c;

		var qrid2 = qrid1 + char[value1] + char[value2];

		data.push([
			qrid2, card_type, "TRUE"
			])
	}

	// console.log(data)
	var sql = 'INSERT INTO gadget_cards (card_qrid, card_type_id, is_printed) VALUES ?';

		db.query(sql,[data], function(error, results, fields){
			// for print value sql query
			// console.log(this.sql); 
			if (error) {
				console.log(error);
				res.json({
					error: 1,
					message: 'There are some error with query'
				})
			}else{
				if(results.insertId){
					console.log('new card created')
				}
				else{
					console.log('failed to create new card')
				}
				next();
			}
		});
}


// check qrid from database
exports.check_qrid = function(req,res, next){

	var qrid = req.body.card_qrid;
	console.log(qrid);

	var char = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
	
	var x1 = 0;
	var x2 = 0;

	for(var i = 0; i < 18; i++)
	{
		var key = char.indexOf(qrid[i]);
		if((i + 1) % 2 == 1)
		{
			x1 += (key << 2) + 1;
		}
		else
		{
			x2 += (key << 1);
		}
	}        

	var c = char.length;

	var val1 = (x1 + x2) % c;
	var val2 = x2 % c;

	if(char[val1] == qrid[18] && char[val2] == qrid[19])
	{
		// return true;
		console.log('data match');
	}
	else
	{
		// return false;
		console.log('data is not valid');
	}

	var sql = 'SELECT id, card_qrid FROM gadget_cards WHERE card_qrid = ?';

	db.query(sql, [qrid], function(error, results, fields){
		console.log(this.sql);
		console.log(results);
		if (error) {
			console.log(error.sqlMessage);
			res.json({
				error : 1,
				message: 'There are some error with query'
			})
		}else{
			if (results == "") {
				res.json({
					error: 1,
					message: 'Data qrid is not valid'
				})
			}else{
				res.json({
					message: 'Data is valid'
				})
			}
		}
	});
}