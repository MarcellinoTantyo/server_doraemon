'use strict';

const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

const db = require('../helper/db');
var jwt = require('jsonwebtoken');
var ejwt = require('express-jwt');
var jwt_helper = require('../helper/jwt');
var error_helper = require('../helper/error');
var request = require('request');

exports.login = function(req, res, next){
	var facebook_id = req.body.facebook_id;
	//var username = req.body.username;
	//var email = req.body.email;
	var access_token = req.body.access_token;
	
	request.get('https://graph.facebook.com/v2.2/me?access_token='+access_token+'&fields=id,name', function (error, response, body) {
		if (error) {
			return console.error('failed:', error);
		}else{
	    	//console.log(typeof(body)) for know the type data
	    	var result = JSON.parse(body)
	    	//console.log('Data : ', body);
	    	//console.log('ID : ', result.id);
	    	if (result.id == facebook_id) {

	    		var sql = 'SELECT user_id, facebook_id, jwt FROM users WHERE facebook_id = ? LIMIT 1';

	    		db.query(sql,[facebook_id], function(error, results, fields){

	    			if (error) {
	    				console.log(error.sqlMessage);
	    				res.json({
	    					error: 1,
	    					message: 'There are some error with query'
	    				})
	    			}else{
	    				var payload = {
	    					user_id : results[0].user_id,
	    				};
	    				console.log(payload);
	    				var token2 = jwt.sign(payload, _config.jwt.secret, {expiresIn : _config.jwt.exp});
	    				res.locals.token2 = token2; 
	    				res.locals.facebook_id = results.length ? results[0].facebook_id : 0;
	    				next();
	    			}
	    		});
	    	}else{
	    		res.json({
	    			error : 1,
	    			message : 'Failed'
	    		})
	    	}
	    }
	});
}

exports.save_jwt = function(req, res, next){

	// console.log(res.locals.facebook_id);
	var facebook_id = res.locals.facebook_id;
	var jwt2 = res.locals.token2;

	if(facebook_id){
		// update jwt
		var sql = 'UPDATE users SET jwt = ? WHERE facebook_id = ?';

		db.query(sql,[jwt2, facebook_id], function(error, results, fields){
			
			if (error) {
				console.log(error.sqlMessage);
				res.json({
					error: 1,
					message: 'There are some error with query'
				})
			}else{
				if(results.affectedRows > 0){
					console.log('updated')
				}
				else{
					console.log('update failed')
				}
				next();
			}
		});
	}
	else{
		// register new user
		var data = {
			facebook_id : req.body.facebook_id,
			jwt: jwt2
		}

		var sql = 'INSERT INTO users SET ?';

		db.query(sql,[data], function(error, results, fields){
			if (error) {
				console.log(error.sqlMessage);
				res.json({
					error: 1,
					message: 'There are some error with query'
				})
			}else{
				if(results.insertId){
					console.log('new user created')
				}
				else{
					console.log('failed to create new user')
				}
				next();
			}
		});
	}
}