'use strict'

const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

const db = require('../helper/db');
var jwt = require('jsonwebtoken');
var ejwt = require('express-jwt');
var jwt_helper = require('../helper/jwt');
var error_helper = require('../helper/error');

exports.get_app_version = function(req, res, next){

	var sql = 'SELECT version FROM app_version';

	db.query(sql, function(error, results, fields){
		console.log(results);
		if (error) {
			console.log(error.sqlMessage);
			res.json({
				error: 1,
				message: 'There are some error with query'
			})
		}else{
			res.json({
				version : results[0].version
			})	
		}
	});
}