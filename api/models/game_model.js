'use strict'

const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

const db = require('../helper/db');
var jwt = require('jsonwebtoken');
var ejwt = require('express-jwt');
var jwt_helper = require('../helper/jwt');
var error_helper = require('../helper/error');
var randomstring = require('randomstring');
var moment = require('moment');
var timediff = require('timediff');

exports.insert_seed = function(req, res, next){

	var random = randomstring.generate({
		length : 30,
		capitalization : 'uppercase'
	});

	var data = {
		stage : req.body.stage,
		seed : random,
		country : req.body.country
	}
	
	var sql = 'INSERT INTO game_seeds SET ?';

	db.query(sql,[data],function(error, results, fields){
		
		if (error) {
			console.log(error.sqlMessage);
			res.json({
				error : 1,
				message : 'There are some error with query'
			})
		}else{
			if (results.insertId) {
				console.log('New game seed has been created');
			}else{
				console.log('Failed to make game seed');
			}
			next();
		}
	});
}

exports.select_game_seed = function(req, res, next){

	var stage = req.body.stage;

	var sql = 'SELECT seed FROM game_seeds WHERE stage = ? ORDER BY RAND() LIMIT 1';

	db.query(sql, [stage], function(error, results, fields){
		//console.log(results);
		if (error) {
			console.log(error.sqlMessage);
			res.json({
				error: 1,
				message: 'There are some error with query'
			})
		}else{
			res.locals.result = results[0].seed;
			next();	
		}
	});

}

exports.insert_game_history = function(req, res, next){

	var result_game_seed = res.locals.result;
	//console.log(result_game_seed);
	var time = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
	var user_id = req.user.user_id;
	//console.log(user_id);
	
	var data = {
		seed : result_game_seed,
		user_id : user_id,
		start_time : time
	}

	var sql = 'INSERT INTO game_history SET ?';

	db.query(sql,[data],function(error, results, fields){
		//console.log(this.sql);
		if (error) {
			console.log(error.sqlMessage);
			res.json({
				error : 1,
				message : 'There are some error with query'
			})
		}else{
			if (results.insertId) {
				console.log('new game history inserted');
			}else{
				console.log('failed to insert data');
			}
			next();
		}
	});
}

exports.select_min_accomplish_time = function(req, res, next){

	var sql = 'SELECT min_accomplish_time FROM game_seeds JOIN game_history ON game_seeds.seed = game_history.seed';

	db.query(sql,function(error, results, fields){
		
		if (error) {
			console.log(error.sqlMessage);
			res.json({
				error : 1,
				message : 'There are some error with query'
			})
		}else{
			res.locals.min_accomplish_time = results[0].min_accomplish_time;
			next();
		}
	});
}

exports.playing_time = function(req,res,next){

	var sql = 'SELECT start_time FROM game_history WHERE user_id = ? AND game_id = ?';

	var user_id = req.user.user_id;
	var game_id = req.body.game_id;
	var score = req.body.score;

	var end_time = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
	//console.log(end_time);

	var min_time = res.locals.min_accomplish_time;
	//console.log(min_time);

	db.query(sql,[user_id, game_id], function(error, results, fields){
		//console.log(results);
		if (error) {
			console.log(error.sqlMessage);
			res.json({
				error : 1,
				message : 'Error query'
			})
		}else{
			if (results != "") {
				var second = timediff(results[0].start_time,end_time,'S');
				//second["seconds"] => for convert object to int
				if (second["seconds"] >= min_time) {

					var sql2 = 'UPDATE game_history SET end_time = ?, score = ? WHERE game_id =?';

					db.query(sql2,[end_time, score, game_id], function(error, results, fields){

						if (error) {
							console.log(error.sqlMessage);
							res.json({
								error: 1,
								message: 'There are some error with query'
							})
						}else{
							if(results.affectedRows > 0){
								console.log('game history updated')
							}
							else{
								console.log('update game history failed')
							}
							res.locals.game_id = game_id;
							res.locals.user_id = user_id;
							next();
						}
					});
				}else{
					res.json({
						message : 'Data not valid or error'
					})
				}
			}else{
				res.json({
					message : 'Data not valid or error'
				})
			}
		}
	})

}