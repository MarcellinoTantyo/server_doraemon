'use strict'

const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

const db = require('../helper/db');
var jwt = require('jsonwebtoken');
var ejwt = require('express-jwt');
var jwt_helper = require('../helper/jwt');
var error_helper = require('../helper/error');
var moment = require('moment');
var timediff = require('timediff');

exports.select_data = function(req, res, next){

	var user_id = res.locals.user_id;
	var game_id = res.locals.game_id;

	var sql = 'SELECT game_id, stage, start_time, end_time, user_id, score FROM game_seeds JOIN game_history ON game_seeds.seed = game_history.seed WHERE game_id = ? AND user_id = ?';

	db.query(sql, [game_id, user_id], function(error, results, fields){
		//console.log(results);
		if (error) {
			console.log(error.sqlMessage);
			res.json({
				error: 1,
				message: 'There are some error with query'
			})
		}else{
			// res.locals.stage = results;
			// next();
			if (results != "") {

				var playtime = timediff(results[0].start_time, results[0].end_time, 'S');
				var month = moment(results[0].end_time).format('MMMM');

				var data = {
					game_id	: game_id,
					stage	: results[0].stage,
					month	: month,
					user_id	: user_id,
					playtime: playtime["seconds"],
					score	: results[0].score
				}

				var sql2 = 'INSERT INTO leaderboard SET ?';

				db.query(sql2,[data],function(error, results, fields){
				//console.log(this.sql);
					if (error) {
						console.log(error.sqlMessage);
						res.json({
							error : 1,
							message : 'There are some error with query'
						})
					}else{
						if (results.insertId) {
							res.json(data)
						}else{
							res.json({
								message : 'Failed to insert data'
							})
						}
					}
				});

			}else{
				res.json({
					message : 'Invalid data'
				})
			}
		}
	});
}