'use strict';

const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

const db = require('../helper/db');
var jwt = require('jsonwebtoken');
var ejwt = require('express-jwt');

var jwt_helper = require('../helper/jwt');
var validator_helper = require('../helper/validator');

//model
var login_model = require('../models/login_model');

// ======================================================

exports.check_jwt = [
ejwt({
	secret: _config.jwt.secret,
	credentialsRequired: true
}),
jwt_helper.error_handler,
function(req, res, next){
	if (req.user.user_id) {
		next();
	}else{
		res.send({
			error: 1,
			message: 'User id not found on token'
		});
		next();
	}
}
]

// =======================================================

/**
 * validation for login
 * body : facebook_id, username, email
 */
exports.check_login = [
sanitize('facebook_id').trim(),
check('facebook_id')
.not().isEmpty().withMessage('Facebook ID is required'),
// check('username')
// .not().isEmpty().withMessage('Username is required'),
// sanitize('email').trim(),
// check('email')
// .not().isEmpty().withMessage('Email is required')
// .isEmail().withMessage('Must be an email'),
// sanitize('access_token').trim(),
// check('access_token')
// .not().isEmpty().withMessage('Access token required'),
validator_helper.error_handler
]

// ========================================================

/**
 * login
 */

exports.login = [
exports.check_login,
// function(req,res,next){
// 	var access_token = req.body.access_token;

// 	res.locals.access_token = access_token;
// 	next();
// },
login_model.login,
login_model.save_jwt,
function(req, res){
	res.json({
		message: 'Successfully Login',
		jwt : res.locals.token2
	})
}
]