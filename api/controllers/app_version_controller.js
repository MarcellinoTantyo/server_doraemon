'use strict';

const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

const db = require('../helper/db');
var jwt = require('jsonwebtoken');
var ejwt = require('express-jwt');

var jwt_helper = require('../helper/jwt');
var validator_helper = require('../helper/validator');

// model
var app_version_model = require('../models/app_version_model');

// =====================================================================

exports.app_version = [
app_version_model.get_app_version
]