'use strict';

const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

const db = require('../helper/db');
var jwt = require('jsonwebtoken');
var ejwt = require('express-jwt');

var jwt_helper = require('../helper/jwt');
var validator_helper = require('../helper/validator');

// model
var game_model = require('../models/game_model');
var leaderboard_model = require('../models/leaderboard_model');

// =================================================================

exports.check_jwt = [
ejwt({
	secret: _config.jwt.secret,
	credentialsRequired: true
}),
jwt_helper.error_handler,
function(req, res, next){
	if (req.user.user_id) {
		next();
	}else{
		res.send({
			error: 1,
			message: 'User id not found on token'
		});
		next();
	}
}
]

// =================================================================

exports.game_seed = [
game_model.insert_seed
]

exports.game_start = [
jwt_helper.jwt_exist,
game_model.select_game_seed,
function(req, res, next){
	var result = res.locals.result;
	next();
},
game_model.insert_game_history,
function(req, res){
	res.send({game_seed : res.locals.result})
}
]

exports.game_end = [
jwt_helper.jwt_exist,
game_model.select_min_accomplish_time,
function(req, res, next){
	var min_accomplish_time = res.locals.min_accomplish_time;
	next();
},
game_model.playing_time,
function(req, res, next){
	var game_id = res.locals.game_id;
	var user_id = res.locals.user_id;
	next();
},
leaderboard_model.select_data
]