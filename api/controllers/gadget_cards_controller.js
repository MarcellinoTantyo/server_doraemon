'use strict';

const{check, validationResult} = require('express-validator/check');
const{matchedData, sanitize} = require('express-validator/filter');

const db = require('../helper/db');
var jwt = require('jsonwebtoken');
var ejwt = require('express-jwt');

var jwt_helper = require('../helper/jwt');
var validator_helper = require('../helper/validator');

//model
var gadget_cards_model = require('../models/gadget_cards_model');

// ===============================================================

exports.check_jwt = [
ejwt({
	secret: _config.jwt.secret,
	credentialsRequired: true
}),
jwt_helper.error_handler,
function(req, res, next){
	if (req.user.user_id) {
		next();
	}else{
		res.send({
			error: 1,
			message: 'User id not found on token'
		});
		next();
	}
}
]

// ===============================================================

exports.check_gadget_cards = [
sanitize('batch').trim(),
check('batch')
.not().isEmpty().withMessage('Batch is required'),
sanitize('card_type').trim(),
check('card_type')
.not().isEmpty().withMessage('Card type is required'),
sanitize('qty').trim(),
check('qty')
.not().isEmpty().withMessage('Qty is required')
]

// ===============================================================

exports.gadget_cards = [
jwt_helper.jwt_exist,
exports.check_gadget_cards,
gadget_cards_model.qrid2,
function (req, res){
	res.send({message : 'qrid generated'})
}
]

exports.check_qrid = [
jwt_helper.jwt_exist,
gadget_cards_model.check_qrid
]