var express = require('express'),
app = express(),
fs = require('fs'),
path = require('path'),
port = process.env.PORT || 12345,
bodyParser = require('body-parser'),
mysql = require('mysql');
var morgan = require('morgan');

var accessLogStream = fs.createWriteStream(path.join(__dirname,'access.log'), {flags: 'a'})

app.use(morgan('dev'));

//jwt
var expressjwt = require('express-jwt');
var jwt2 = require('jsonwebtoken');
global._config = require('./config');

app.set('jwt_secret', _config.jwt.secret);
app.set('jwt_exp', _config.jwt.exp);

//body parser
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//importing route
var routes = require('./api/routes/all');
routes(app);

// 404
app.use(function(req, res){
	res.status(404).send({error: '404', url: req.originalUrl})
});

app.listen(port);

console.log('Doraemon server started on: ' + port);